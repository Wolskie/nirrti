#!/bin/bash
#-----------------------------------------------------------------
#
# Author:  Mark Hahl
# Version: 0.0.0
# License: GPLv2+
#
# Usage: Create a file in cfg/ set this scrips
#    CFG_DIR variable below to that directory.
#    In each config file ensure you have the line
#    "#Location_: " (note the space) in the config file.
#    This tells tr script where to make a link to the file.
#
#-----------------------------------------------------------------

source ./common.sh

# Loop through every file in the directory
# and fetch the line begining with: "#_Location: "
#
CFG_DIR="/etc/nirrti/cfg" 
SEARCH=$(find $CFG_DIR -type f)

for cfgf in $SEARCH
do

    # Strip the secion that says #_LOCATION: from
    # he line. The remaining text is where the file should
    # get replaced
    #
    step "Processing $cfgf"
    LOCATION=$(sed -n "/^#_Location/p" "$cfgf" | sed "s/#_Location: //g")
 
    # Check to see if the file exists, wether its a normal file or
    # a symlink, of so then perform the backup / then linking.
    #
    if [ -f "$LOCATION" ] || [ -L "$LOCATION" ];
    then

        # If the file is a symlink then unlink it then relink it.
        # this does not backup anything or do any other checks.
        #
        if [ -L "$LOCATION" ];
        then
            try unlink "$LOCATION" 2>> logs/distrib.log
            try ln -s "$cfgf" "$LOCATION" 2>> logs/distrib.log
        else

            # If the file is not a synlink then create a backup called
            # file.bak then remove the old file then link this one.
            #
            try mv "$LOCATION" "$LOCATION.$(date +%s)" 2>> logs/distrib.log
            try ln -s "$cfgf" "$LOCATION"  2>> logs/distrib.log
        fi
    else
        # The file doesnt exist, link it now.
        #
        try ln -s "$cfgf" "$LOCATION" 2>> logs/distrib.log then; then
    fi
    next    
done
