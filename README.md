nirrti
=======

This script looks for all files in the cfg/ directory
and find the line which begins with # Location_:
and symlinks the file to that location.

usage
=======

Usage: Create a file in cfg/ set this scrips
CFG_DIR variable below to that directory.
In each config file ensure you have the line
"#Location_: " (note the space) in the config file.
This tells the script where to make a link to the file.

